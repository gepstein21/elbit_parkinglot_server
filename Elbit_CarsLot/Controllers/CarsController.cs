﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BL;
using BL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Elbit_CarsLot.Controllers
{

    [Route("api/Cars")]
    [ApiController]
    public class CarsController : ControllerBase
    {
        // GET: api/Cars
        [HttpGet]
        public IEnumerable<CarLotCar> Get()
        {
            return CarsRepository.ShowAll();
        }

        
        // GET: api/Cars/5
        [HttpGet("{id}", Name = "Get")]
        public CarLotCar Get(string plateNumber)
        {
            return CarsRepository.View(plateNumber);
        }

        // POST: api/Cars
        [HttpPost]
        public IEnumerable<CarLotCar> Post([FromBody] CarLotCar car)
        {
            return CarsRepository.Add(car);
        }

        // PUT: api/Cars/5
        //[HttpPut("{id}")]
        //http://localhost:55225/api/Cars?plateNumber="abc"
        public IEnumerable<CarLotCar> Put([FromQuery]string plateNumber, [FromBody] CarLotCar car)
        {
            return CarsRepository.Update(plateNumber, car);
        }

        
        //http://localhost:55225/api/Cars?plateNumber="abc"
        [HttpDelete]
        public IEnumerable<CarLotCar> Delete(string plateNumber)
        {
            return CarsRepository.Remove(plateNumber);
        }
    }
}
