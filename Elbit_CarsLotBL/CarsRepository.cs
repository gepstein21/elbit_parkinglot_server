﻿using BL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace BL
{
    public static class CarsRepository
    {
        public static List<CarLotCar> CarsLotItems = new List<CarLotCar>();

        public static List<CarLotCar> ShowAll()
        {
            return CarsLotItems;
        }
        public static IEnumerable<CarLotCar> Add (CarLotCar car)
        {
            if (!CarsLotItems.Any(item => item.PlateNumber == car.PlateNumber))
            {
                CarsLotItems.Add(car);
            }
            return CarsLotItems;
        }

        public static IEnumerable<CarLotCar> Remove (CarLotCar car)
        {
            int? itemIndex = CarsLotItems.IndexOf(car);
            if (itemIndex.HasValue)
            {
                CarsLotItems.RemoveAt(itemIndex.Value);
            }
            return CarsLotItems;
        }
        public static IEnumerable<CarLotCar> Remove(string plateNumber)
        {
            CarLotCar car = CarsLotItems.FirstOrDefault(item => item.PlateNumber == plateNumber);
            if (car != null)
            {
                return Remove(car);
            }
            return CarsLotItems;
        }

        public static IEnumerable<CarLotCar> Update(string plateNumber, CarLotCar car)
        {
            CarLotCar selectedCar = CarsLotItems.FirstOrDefault(item => item.PlateNumber == plateNumber);
            if (selectedCar != null)
            {
                selectedCar.setValues(car);
            }
            return CarsLotItems;
        }
        public static IEnumerable<CarLotCar> Update (CarLotCar car)
        {
            CarLotCar selectedCar = CarsLotItems.FirstOrDefault(item => item.PlateNumber == car.PlateNumber);
            if (selectedCar != null)
            {
                selectedCar = new CarLotCar();
            }
            return CarsLotItems;
        }

        public static CarLotCar View (string plateNumber)
        {
            return CarsLotItems.FirstOrDefault(item => item.PlateNumber == item.PlateNumber);
        }
    }
}
