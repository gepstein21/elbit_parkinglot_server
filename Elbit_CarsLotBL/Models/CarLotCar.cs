﻿using Elbit_CarsLotBL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;

namespace BL.Models
{
    public class CarLotCar : Car
    {
        [JsonProperty("receivingDate")]
        public DateTime ReceivingDate { get; set; }
        [JsonProperty("wantedPrice")]
        public int WantedPrice { get; set; }

        public CarLotCar() : base() { }
        public CarLotCar(CarLotCar car) : base(car) 
        {
            ReceivingDate = car.ReceivingDate;
            WantedPrice = car.WantedPrice;
        }
        internal void setValues(CarLotCar car)
        {
            ModelID = car.ModelID;
            TypeModel = car.TypeModel;
            YearModel = car.YearModel;
            NumberOfKM = car.NumberOfKM;
            WantedPrice = car.WantedPrice;
        }
    }
}
