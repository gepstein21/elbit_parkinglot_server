﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace BL.Models
{
    public class CarModel
    {
        [JsonProperty("id")]
        public int ID { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }

        public CarModel() { }
    }
}
