﻿using BL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Elbit_CarsLotBL.Models
{
    public class Car
    {
        [JsonProperty("plateNumber")]
        public string PlateNumber { get; set; }
        [JsonProperty("modelID")]
        public /*CarModel*/int ModelID{ get; set; }
        [JsonProperty("typeModel")]
        public string? TypeModel { get; set; }
        [JsonProperty("yearModel")]
        public int YearModel { get; set; }
        [JsonProperty("numberOfKM")]
        public int NumberOfKM { get; set; }

        public Car() { }

        public Car(Car car)
        {
            PlateNumber = car.PlateNumber;
            ModelID= car.ModelID;
            TypeModel = car.TypeModel;
            YearModel = car.YearModel;
            NumberOfKM = car.NumberOfKM;
        }
    }
}
