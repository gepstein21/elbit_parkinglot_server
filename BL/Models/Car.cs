﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BL.Models
{
    public class Car
    {
        public int ID { get; set; }
        public string TypeName { get; set; }

        public string? TypeModel { get; set; }


    }
}
